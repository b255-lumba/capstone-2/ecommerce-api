// Setting up dependencies
const express = require('express');
const router = express.Router();

// Importing the "product" controller from the "controllers" folder
const productController = require('../controllers/product');

// Importing the auth middleware
const auth = require('../auth');

// SETTING UP THE ROUTERS

// Route for creating a new product (admin only, call auth.verify for admin)
router.post('/', auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization);
    if (userData.isAdmin == true) {
        console.log(req.body);
        productController.addProduct(req.body).then((resultFromController) => {
            res.send(resultFromController);
        });
    } else {
        res.send(false);
    }
});

// Route for retrieving all products
router.get('/all', (req, res) => {
    // Use the getAllProducts method from the "productController" to get all products
    productController.getAllProducts().then((resultFromController) => {
        res.send(resultFromController);
    });
});

// Route for retrieving all active products
router.get('/', (req, res) => {
    // Use the getAllActive method from the "productController" to get all active products
    productController.getAllActive().then((resultFromController) => {
        res.send(resultFromController);
    });
});

// Route for retrieving a specific product
router.get('/:productId', (req, res) => {
    // For checking the course id in console
    console.log(req.params.productId);
    // Use the getProduct method from the "productController" to get a specific product which passes the params of the request (req.params) to the corresponding controller
    productController.getProduct(req.params).then((resultFromController) => {
        res.send(resultFromController);
    });
});

// Route for updating a product (admin only, call auth.verify for admin)
// NOTE TO SELF: can change /update to /:productId
router.put('/:productId', auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization);
    if (userData.isAdmin == true) {
        // Use the updateProduct method from the "productController" to update a specific product which passes the body and params of the request (req.body, req.params) to the corresponding controller
        productController.updateProduct(req.body, req.params).then((resultFromController) => {
            res.send(resultFromController);
        });
    } else {
        res.send(false);
    }
});


// Route for archiving a product (admin only, call auth.verify for admin)
router.put('/:productId/archive', auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization);
    if (userData.isAdmin == true) {
        // Use the archiveProduct method from the "productController" to archive a specific product which passes the params of the request (req.params) to the corresponding controller
        productController.archiveProduct(req.params).then((resultFromController) => {
            res.send(resultFromController);
        });
    } else {
        res.send(false);
    }
});


// Allows export of the "product" router to be used in the "index.js" file
module.exports = router;