// Setting up the dependencies
const express = require("express");
const router = express.Router();

// Importing the "order" controller from the "controllers" folder
const orderController = require("../controllers/order");

// Importing the auth middleware
const auth = require("../auth");

// SETTING UP THE ROUTERS

// Route for creating an order for logged in user
router.post("/", auth.verify, (req, res) => {
    // Use the addOrder method from the "orderController" to create an order which passes the body of the request (req.body) to the corrensponding controller
    orderController.addOrder(req.body).then((resultFromController) => {
        res.send(resultFromController);
    });
});

// Route for retrieving user orders
router.get("/:userId", auth.verify, (req, res) => {
    // Use the getOrders method from the "orderController" to get the user orders which passes the body of the request (req.body) to the corrensponding controller
    orderController.getOrders(req.params.userId).then((resultFromController) => {
        res.send(resultFromController);
    });
});

// Allow export of the router
module.exports = router;