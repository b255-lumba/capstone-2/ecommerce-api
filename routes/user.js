// Setting up dependencies
const express = require('express');
const router = express.Router();

// Importing the "user" controller from the "controllers" folder
const userController = require('../controllers/user');

// Importing the auth middleware
const auth = require('../auth');

// SETTING UP THE ROUTERS

// Route for checking if user email already exists in the database
router.post('/checkEmail', (req, res) => {
    // Invoke the checkEmailExists method from the userControllers
    userController.checkEmailExists(req.body).then((resultFromController) => {
        res.send(resultFromController);
    });
});

// Route for registering a user on ecommerce
router.post('/register', (req, res) => {
    // Use the registerUser method from the "userController" to register a user which passes the body of the request (req.body) to the corrensponding controller
    userController.registerUser(req.body).then((resultFromController) => {
        res.send(resultFromController);
    });
});

// Route for logging in a user
router.post('/login', (req, res) => {
    // Invoke the loginUser method from the userController
    userController.loginUser(req.body).then((resultFromController) => {
        res.send(resultFromController);
    });
});


// Route for retrieving user details, only allowed if the user is logged in/authenticated
router.get('/details', auth.verify, (req, res) => {
    // Invoke the "decode" method from the auth middleware
    const userData = auth.decode(req.headers.authorization);
    // Invoke the getProfile method from the userController
    // userController.getProfile({ userId: userData.id }) - USING THIS WILL ONLY ALLOW THE USER TO VIEW THEIR OWN DETAILS
    userController.getProfile({ userId: req.body.id })
    // USING THIS WILL ALLOW THE USER TO VIEW ANY USER'S DETAILS AS LONG AS THEY KNOW THE USER ID
        .then((resultFromController) => {
            res.send(resultFromController);
        });
});

// Route for setting user as admin, only allowed to be accessed by the admin
router.put('/setAdmin/:userId', auth.verify, (req, res) => {
    // Check if the user accessing the route is an admin
    const userData = auth.decode(req.headers.authorization);
    if (userData.isAdmin == true) {
        // Console log the request body
        console.log(req.body);
        // Invoke the setAdmin method from the userController
        userController.setAdmin(req.params, req.body)
            .then((resultFromController) => {
                res.send(resultFromController);
            });
    } else {
        res.send({ message: 'You are not authorized to access this route' });
    }
});

// Allow export of the "user" router to be used in the "index.js" file
module.exports = router;