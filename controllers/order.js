// Importing the models
const Order = require('../models/Order');

// Create a new order
module.exports.addOrder = (reqBody) => {
    // Create a variable "newOrder" and instantiates a new "Order" object using the mongoose model
    let newOrder = new Order({
        userId: reqBody.userId,
        products: reqBody.products,
        totalAmount: reqBody.totalAmount,
        purchasedOn: new Date()
    });

    // Saves the created object to our database
    return newOrder.save().then((order, error) => {
        // Order creation failed
        if (error) {
            return false;

            // Order creation successful
        } else {
            return true
        }
    })
};

// Retrieve user's orders
module.exports.getOrders = (userId) => {
    return Order.find({ userId: userId }).then(result => {
        return result;
    })
};