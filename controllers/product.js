// Importing the product model
const Product = require('../models/Product');

// Creating a new product which is only allowed for admin
module.exports.addProduct = (reqBody) => {
    // Create a variable "newProduct" to store the new product details and instantiate the "Product" model
    let newProduct = new Product({
        name: reqBody.name,
        description: reqBody.description,
        price: reqBody.price
    });

    // Save the new product to the database
    return newProduct.save().then((product, error) => {
        // If there is an error, return false
        if (error) {
            return false;
        } else {
            return true;
        }
    });
};

// Retrieving all products
module.exports.getAllProducts = () => {
    return Product.find({}).then(result => {
        return result;
    })
};

// Retrieving all ACTIVE products
module.exports.getAllActive = () => {
    return Product.find({isActive : true}).then(result => {
        return result;
    })
};

// Retrieving a specific product
module.exports.getProduct = (reqParams) => {
    return Product.findById(reqParams.productId).then(result => {
        return result;
    })
};

// Updating a specific product information, only allowed for admin
module.exports.updateProduct = (reqBody, reqParams) => {
    // Specify the fields/properties to be updated
    let updateProduct = {
        name: reqBody.name,
        description: reqBody.description,
        price: reqBody.price
    };

    return Product.findByIdAndUpdate(reqParams.productId, updateProduct).then((product, error) => {
        // Product update failed
        if (error) {
            return false;
        } else {
            return true;
        }
    });
};

// Archiving a specific product, only allowed for admin
module.exports.archiveProduct = (reqParams) => {
    // Specify the field/property to be updated
    let updateActiveField = {
        isActive: false
    };

    return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((result, error) => {
        // Product not archived
        if (error) {
            return false;

        // Product archived successfully
        } else {
            return true;
        }
    });
};