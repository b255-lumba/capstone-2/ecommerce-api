// Importing the bcrypt module
const bcrypt = require('bcrypt');

// Importing the models for Ecommerce API
const User = require('../models/User');
const Product = require('../models/Product');
const Order = require('../models/Order');

// Importing the auth middleware
const auth = require('../auth');

// CONTROLLER FUNCTIONS

// Checking if email already exists
module.exports.checkEmailExists = (reqBody) => {
    // The result is sent back to the frontend via the "then" method in the "userRoutes" file using the mongoose find() method
    return User.find({ email: reqBody.email }).then((result) => {
        // If the result is not empty, the email already exists
        if (result.length > 0) {
            return true;
            // if not, the user is not yet registered in the database
        } else {
            return false;
        }
    });
};

// Registering a new user for ecommerce API
module.exports.registerUser = (reqBody) => {
    // Create a variable "newUser" to store the new user details and instantiate the "User" model
    let newUser = new User({
        email: reqBody.email,
        // Hash the password using the bcrypt library
        password: bcrypt.hashSync(reqBody.password, 10)
    });

    // Save the new user to the database
    return newUser.save().then((user, error) => {
        // If there is an error, return false
        if (error) {
            return false;
        } else {
            // Registered successfully
            return true;
        }
    });
};

// Logging in & authenticating the user
module.exports.loginUser = (reqBody) => {
    // Find the user by the email using the findOne() method
    return User.findOne({ email: reqBody.email }).then(result => {
        // If the user is not found, return false
        if (result === null) {
            return false;
        } else {
            // If the user is found, compare the password using the bcrypt library by creating a variable isPasswordCorrect and passing the password from the request body and the password from the database
            let isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

            // If the password is correct/match, generate a token using the "createAccessToken" method from the "auth" middleware and pass the user details then return the object to the frontend
            if (isPasswordCorrect) {
                return {access : auth.createAccessToken(result)};
            } else {
                // when passwords don't match, return false
                return false;
            }
        }
    });
};

// Retrieving user details
module.exports.getProfile = (data) => {
    // Find user by ID by invoking the "findById" method
    return User.findById(data.userId).then(result => {
        // If the user is not found, return false
        if (result === null) {
            return false;
        } else {
            // If the user is found, return the user details as an empty string
            result.password = '';
            return result;
        }
    });
};

// Update user details - set user as admin
module.exports.setAdmin = (reqParams, reqBody) => {
    // Specify the fields to be updated
    let updateFields = {
        isAdmin: reqBody.isAdmin
    };

    // Find the user by ID and update the fields
    return User.findByIdAndUpdate(reqParams.userId, updateFields).then(result => {
        // If the user is not found, return false
        if (result === null) {
            return false;
        } else {
            // If the user is found, return true
            return true;
        }
    });
};
