// Setting up the dependencies
const mongoose = require('mongoose');

// Scheme for Order on Ecommerce
const orderSchema = new mongoose.Schema({
    userId: {
        type: String,
        // Must be associated with user who owns the order
        required: [true, "User ID is required"]
    },
    // products - array of objects containing productId and quantity
    products: [
        {
            productId: {
                type: String,
                required: [true, "Product ID is required"]
            },
            quantity: {
                type: Number,
                required: [true, "Quantity is required"]
            }
        }
    ],
    // totalAmount - calculated from the products array
    totalAmount: {
        type: Number,
        required: [true, "Total amount is required"]
    },
    // purchasedOn - date when the order was placed, defaults to current timestamp
    purchasedOn: {
        type: Date,
        default: new Date()
    }
});

// Allow export of the model
module.exports = mongoose.model('Order', orderSchema);