// Setting up dependencies
const mongoose = require('mongoose');

// Schema for User on Ecommerce
const userSchema = new mongoose.Schema({
    email: {
        type: String,
        required: [true, "Email is required"]
    },
    password: {
        type: String,
        required: [true, "Password is required"]
    },
    isAdmin : {
        type: Boolean,
        default: false
    }
});

// Allow export of the model
module.exports = mongoose.model('User', userSchema);