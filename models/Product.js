// Settng up dependencies
const mongoose = require('mongoose');

// Schema for Product on Ecommerce
const productSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, "Product name is required"]
    },
    description: {
        type: String,
        required: [true, "Product description is required"]
    },
    price: {
        type: Number,
        required: [true, "Product price is required"]
    },
    isActive: {
        type: Boolean,
        default: true
    },
    createdOn: {
        type: Date,
        default: new Date()
    }
    // NOT PART OF REQUIREMENTS, for future include REVIEWS and RATINGS featuring UserId and Rating/Review
});

// Allow export of the model
module.exports = mongoose.model('Product', productSchema);