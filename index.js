// Setting up dependencies for index.js
const express = require('express');
const mongoose = require('mongoose');

// Importing the routes
const userRoutes = require('./routes/user');
const productRoutes = require('./routes/product');
const orderRoutes = require('./routes/order');

// Setting up the app
const app = express();

// Setting up the database connection
mongoose.connect('mongodb+srv://jlumbajrph:Admin.123@test.ezjezum.mongodb.net/?retryWrites=true&w=majority', {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

mongoose.connection.once('open', () => {
    console.log('Now connected to MongoDB Atlas');
});

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// Setting up the EcommerceAPI routes

// Importing the "user" route from the "routes" folder
app.use('/users', userRoutes);

// Importing the "product" route from the "routes" folder
app.use('/products', productRoutes);

// Importing the "order" route from the "routes" folder
app.use('/orders', orderRoutes);

// Setting up the server and listening to port
if (require.main === module) {
    app.listen(process.env.PORT || 3000, () => {
        console.log(`EcommerceAPI is now online on port ${process.env.PORT || 3000}`)
    });
};

// Exporting the app
module.exports = app;